#!/usr/bin/env bash

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root"
  echo "Plese use sudo or su"
  exit 1
fi

# use aptitude in the next steps ...
if [ \! -f $(whereis aptitude | cut -f 2 -d ' ') ]; then
  apt-get install aptitude -y
fi

source ./scripts/functions.sh

ask "Is this a desktop environment?"
IS_DESKTOP=$?

echo
echo "What packages do you want to install?"
echo "1: General CLI"
echo "2: General (Desktop)"
echo "3: Programming"
echo "4: Programming (Desktop)"
echo "5: Games"
echo "List number separated by spaces."
read INPUT_RAW

IFS=' ' read -r -a INPUT <<<"$INPUT_RAW"

for i in "${INPUT[@]}"; do
  case $i in

  1)
    echo "Installing: General CLI"
    ./scripts/general-cli.sh
    ;;
  2)
    echo "Installing: General (Desktop)"
    if [[ "$IS_DESKTOP" -eq 1 ]]; then
      ./scripts/general-desktop.sh
    fi
    ;;
  3)
    echo "Installing: Programming"
    ./scripts/programming-cli.sh
    ;;
  4)
    echo "Installing: Programming (Desktop)"
    if [[ "$IS_DESKTOP" -eq 1 ]]; then
      ./scripts/programming-desktop.sh
    fi
    ;;
  5)
    echo "Installing: Games"
      ./scripts/games.sh
    ;;
  *)
    echo -n "Unknown input ($i)"
    ;;
  esac

done

# update && upgrade
# aptitude update -y
# aptitude upgrade -y

# Generate ssh key
if [ ! -f ~/.ssh/id_rsa ]; then
  ssh-keygen -t ed25519 -C -N "info@robvankeilegom.be"
fi

# clean downloaded and already installed packages
aptitude -v clean

# update-locate-db
# echo "update-locate-db ..."
# updatedb -v
