# Git flow
alias gf='git flow'
alias gff='git flow feature'
alias gffs='git flow feature start'
alias gfff='git flow feature finish'
alias gffp='git flow feature publish'
alias gffc='git flow feature checkout'

alias gfr='git flow release'
alias gfrs='git flow release start'
alias gfrf='git flow release finish'
alias gfrp='git flow release publish'

alias gfh='git flow hotfix'
alias gfhs='git flow hotfix start'
alias gfhf='git flow hotfix finish'
alias gfhp='git flow hotfix publish'

# Laravel
alias pa='php artisan'

# Git
alias gs='git status'
alias gc='git commit -v'
alias gp='git pull --no-edit ; git push'
alias gpa='git push --all ; git push --tags'
alias gd='git checkout develop'

alias ipconfig='ifconfig'

alias c='xclip -selection clipboard'
