#!/usr/bin/env bash

ADOPT=$1

source ./scripts/functions.sh

mkdir -p programs

stow $ADOPT -vSt ~ bash zsh htop

cd git

ask "work env?"
if [[ $? -eq 1 ]]; then
	stow $ADOPT -vSt ~ work
else
	stow $ADOPT -vSt ~ personal
fi

cd ../

ask "desktop env?"
if [[ $? -eq 1 ]]; then
	stow $ADOPT -vSt ~ vscode shortcuts flameshot
	stow $ADOPT -vSt /opt programs
fi

source ~/.zshrc
