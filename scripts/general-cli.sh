#!/usr/bin/env bash


packagelist=(
  # read-write NTFS driver for Linux
  ntfs-3g
  # do not delete main-system-dirs
  safe-rm
  # default for many other things
  build-essential
  make
  # unzip, unrar etc.
  zip
  unzip
  rar
  unrar
  # interactive processes viewer
  htop
  # interactive I/O viewer
  iotop

  rsync
  stow

  # command line clipboard
  xclip
  # more colors in the shell
  grc
  # fonts also "non-free"-fonts
  # -- you need "multiverse" || "non-free" sources in your "source.list" --
  fontconfig
  ttf-freefont
  ttf-mscorefonts-installer
  ttf-bitstream-vera
  ttf-dejavu
  ttf-liberation
  ttf-linux-libertine
  ttf-larabie-deco
  ttf-larabie-straight
  ttf-larabie-uncommon
  ttf-liberation
  xfonts-jmk
  # get files from web
  wget
  curl
  # usefull tools
  nmap
  zsh
)

aptitude install -y ${packagelist[@]} > /dev/null

# set zsh
chsh -s $(which zsh)

# oh-my-zsh
if [ ! -d "~/.oh-my-zsh" ]; then
  sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi