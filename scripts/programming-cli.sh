#!/usr/bin/env bash

source ./scripts/functions.sh

# install new git / ubuntu
add-apt-repository -y ppa:git-core/ppa > /dev/null

# add yarn
installed "yarn"
if [[ $? -eq 1 ]]; then
  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list > /dev/null
fi
# add node.js
installed "nodejs"
if [[ $? -eq 1 ]]; then
  curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash - > /dev/null
fi

aptitude update -y > /dev/null

# Install mysql without interaction
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

packagelist=(
  # repo-tools
  git
  git-flow
  # Python
  python
  python-pip
  # Javascript
  yarn
  nodejs
  # Web
  apache2
  mysql-server
  php
  php-bcmath
  php-bz2
  php-intl
  php-gd
  php-mbstring
  php-mysql
  php-zip
  php-fpm
  supervisor
)

aptitude install -y ${packagelist[@]} > /dev/null

# Make dir if it doesn't exist already
mkdir -p zsh/.oh-my-zsh

# git-flow autocomplete
if [ ! -f "zsh/.oh-my-zsh/git-flow-completion.zsh" ]; then
  wget https://raw.githubusercontent.com/bobthecow/git-flow-completion/master/git-flow-completion.zsh -O zsh/.oh-my-zsh/git-flow-completion.zsh
fi

# Configure npm
# https://docs.npmjs.com/resolving-eacces-permissions-errors-when-installing-packages-globally
mkdir -p ~/.npm-global
npm config set prefix '~/.npm-global'

npm install -g npm  > /dev/null
npm update -g  > /dev/null


# Install composer
if [ ! -f "/usr/bin/composer" ]; then
  curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin
  ln -s /usr/bin/composer.phar /usr/bin/composer
fi

# Install nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash