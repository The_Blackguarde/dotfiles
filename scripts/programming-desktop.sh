#!/usr/bin/env bash

# vscode
snap install --classic code

packagelist=(
  mysql-workbench
)

aptitude install -y ${packagelist[@]} > /dev/null

mkdir -p programs
cd programs

# Postman
if [ ! -d "Postman" ]; then
  wget https://dl.pstmn.io/download/latest/linux64 -O postman.tar.gz > /dev/null
  tar -xf postman.tar.gz
  rm postman.tar.gz
fi

# Git Cola
if [ ! -d "git-cola" ]; then
  aptitude install python3-pyside2.qtnetwork python-pyqt5
  git clone git://github.com/git-cola/git-cola.git > /dev/null
fi

cd ../