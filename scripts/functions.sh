#!/usr/bin/env bash

# use the bash as default "sh", fixed some problems
# with e.g. third-party scripts
#sudo ln -sf /bin/bash /bin/sh

function ask() {
  echo
  read -p "$1 (y/n) " -n 1
  echo
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    return 1
  else
    return 0
  fi
}

function installed() {
  dpkg -s $1 &>/dev/null

  if [ $? -ne 0 ]; then
    return 1
  else
    return 0
  fi
}
