#!/usr/bin/env bash

# Discord
installed "discord"
if [[ $? -eq 1 ]]; then
  wget https://discordapp.com/api/download\?platform\=linux\&format\=deb -O discord.deb
  dpkg -i discord.deb
  rm discord.deb
fi

# Steam
installed "steam"
if [[ $? -eq 1 ]]; then
  wget https://steamcdn-a.akamaihd.net/client/installer/steam.deb -O steam.deb
  dpkg -i steam.deb
  rm steam.deb
fi