#!/usr/bin/env bash

# Theme
add-apt-repository -y ppa:system76/pop

# Spotify
curl -sS https://download.spotify.com/debian/pubkey.gpg | apt-key add -
echo "deb http://repository.spotify.com stable non-free" | tee /etc/apt/sources.list.d/spotify.list

add-apt-repository -y ppa:gnome-terminator

# Typora
wget -qO - https://typora.io/linux/public-key.asc | apt-key add -
add-apt-repository -y 'deb https://typora.io/linux ./'

# Gimp
add-apt-repository -y ppa:otto-kesselgulasch/gimp

# Libreoffice
add-apt-repository -y ppa:libreoffice/ppa

aptitude update -y

packagelist=(
  pop-theme
  spotify
  terminator
  gimp
  libreoffice
  thunderbird
  qbittorrent
  # Markdown editor
  typora
  # Screenshots
  flameshot
)

aptitude install -y ${packagelist[@]} > /dev/null

snap install vlc

# Theme settings
gsettings set org.gnome.desktop.interface icon-theme 'Pop'
gsettings set org.gnome.desktop.interface gtk-theme 'Pop-dark'
gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'

# Move buttons to the left
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:'

# Turn on night light
gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true
gsettings set org.gnome.settings-daemon.plugins.color night-light-temperature 4000
gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-from 0
gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-to 23.983333333333334

# Add user to groups.
# Write isos
sudo adduser $USER disk
# Arduino
sudo adduser $USER dialout